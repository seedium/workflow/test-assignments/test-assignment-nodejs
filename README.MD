## Test assignment
### Seedium

#### Getting started

1. Check all requirements from this file. Then check [API requirements](https://app.swaggerhub.com/apis/Seedium/TestAssignment/1.0.1)
1. Fork the project in GitLab
1. Clone the forked repository
1. Create branch from master with name `test-<email>`. Please specify email on that we have send invite
1. You will start from `src/` folder. All your written code will be there.
1. You can use any nodejs framework that supports by "chai-http" module (Express, Koa, Fastify, NestJs and etc). Or write on the pure http module. It's only up to you.
1. Open file `src/index.js`. Your entry point will be here and export object as showed in comments example. (It's object will be used in tests)
1. After finishing a work, please run `yarn test` command and check that all tests are passed.
1. Push you branch to GitLab
1. Then in GitLab create new "Merge Request" where source branch it will be you `test-<email>` branch and target will be `master` branch.
   If you create the Merge Request manually, please be sure that you are trying to merge into the original repository,
   so in the end in Merge Request you should see something like this `From <your_gitlab_username>/test-assignment:test-<your_email> into seedium-workflow/test-assignment:master`. 
   Title of merge request set `Test work: <email>`.
   In `Reviewer` field please set our developer `@kostyazgara`, then submit merge request.

#### What we expect

1. Using Postgres as main database.
1. Good project structure. Separate controllers, services, data layer
1. Validation requests and responses.
1. All tests should be passed (for separate endpoint)
1. Properly handled errors
1. Implement `POST /posts` and `GET /posts` endpoints

#### Will be a plus

1. Use typescript
1. Use pure `pg` drive or simple query builder like `knex`, without any ORM like `sequelize` or `typeorm`
1. Implementation of cursor pagination
1. Configured CI/CD
1. Other endpoints `GET /posts/{id_post}`, `PATCH /posts/{id_post}` and `DELETE /posts/{id_post}`

#### Before start

1. Do not touch files with tests, expect one thing
    1. For properly test going you should complete 2 functions - `createPosts` and `removePosts`. Please, use comments near there functions for understanding how it should work
1. Feel free to organize your application structure as you want.
1. Feel free to use any libs from npm.
1. For CI/CD configuration, please modify you `migrate` script in `package.json` with right script
   and be sure that your database driver is using the next values for configuration of connection:
   
   ```markdown
   POSTGRES_USER // username for the connection
   POSTGRES_PASSWORD // password for the connection
   POSTGRES_DB // database name
   POSTGRES_HOST // database hostname
   ```

#### Estimations

1. Project and tasks understanding - **10 minutes**
1. Project set up (clone repository, install dependencies) - **20 minutes**
1. Build models and schemas - **30 minutes**
1. Develop controllers - **30 minutes**
1. Develop validation - **30 minutes**
1. Develop business logic (services for processing main functionality, such as get posts, create posts and etc) - **2 hours**
1. Test passing - **30 minutes**
1. Refactoring - **30 minutes**

Total estimated time to accomplishing test assignment - **5 hours**.
If you are feeling that writing tests or completing other endpoints like `GET /posts/{id_post}`, `PATCH /posts/{id_post}`, `DELETE /posts/{id_post}` are taking more time that estimated,
please don't spend more time and send us everything that is ready!

For additional questions, please don't hesitate to contact with me [k.zgara@seedium.io](mailto:k.zgara@seedium.io) (Kostya Zgara, developer of Seedium)
